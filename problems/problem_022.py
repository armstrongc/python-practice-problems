# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    def gear_for_day_yield(is_workday, is_sunny):
        if is_workday:
            yield "laptop"
        if is_workday and not is_sunny:
            yield "umbrella"
        if not is_workday:
            yield "surfboard"
    return list(gear_for_day_yield(is_workday, is_sunny))


print(list(gear_for_day(True, True)))
print(list(gear_for_day(False, True)))
print(list(gear_for_day(True, False)))
print(list(gear_for_day(False, False)))
